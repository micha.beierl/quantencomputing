// RandomNumbersC_Cpp.cpp : Diese Datei enthält die Funktion "main". Hier beginnt und endet die Ausführung des Programms.
//

#include <iostream>

//Für die C-Variante benötigte Includes
#include <time.h>
#include <stdlib.h>

//Für die C++-Variante benötigte Includes
#include <random>

void initCRand();
int cRandomNumber(int iRandMax = 1024);

void initCppRand(std::default_random_engine& engine);
int cppRandomNumber(std::default_random_engine& engine, int iRandMax = 1024);


int main()
{
	initCRand();
	std::default_random_engine engineInstance;
	initCppRand(engineInstance);

	for (int i = 0; i < 10; ++i) {
		std::cout << "c: " << cRandomNumber() << " c++: " << cppRandomNumber(engineInstance) << std::endl;
	}
	int i;
	std::cin >> i;
}

void initCRand() {
	srand(time(NULL));
}

int cRandomNumber(int iRandMax) {
	return rand() % iRandMax;
}

void initCppRand(std::default_random_engine& engine) {
	engine.seed(engine.default_seed);
}

int cppRandomNumber(std::default_random_engine& engine, int iRandMax) {
	std::uniform_int_distribution<int> dist(0, iRandMax);
	return dist(engine);
}