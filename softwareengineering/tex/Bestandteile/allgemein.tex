\chapter{Softwareengineering im Quantencomputing}

\begin{table}[H]
	\centering
	\begin{tabularx}{\textwidth}{llp{12.0cm}}
		\toprule
		Quelle       & Seite & Beschreibung / Kommentar \\
		\midrule
		\multicolumn{3}{c}{\textbf{Zu Allgemeinen Aufgaben und Forderungen}} \\
		\midrule
		\cite{Miranskyy.} &  & Interessant für den allgemeinen Bereich: Introduction \& erstes Beispiel\\
		\cite{Roth.2015} & & Beschreibung und Merkmale von \textit{System of Systems}\\
		\cite{Grumbling.2019}& 135-155 & Progress and Prospects, Kapitel 6: Essential Software Components of a Scalable Quantum Computer\\
		
		\midrule
		\multicolumn{3}{c}{\textbf{Analysis \& Design}}\\
		\midrule
		\cite{Leymann.2019} & 218-230 & \glqq Towards a Pattern Language for Quantum Algorithms\grqq{}\\
		
		\midrule
		\multicolumn{3}{c}{\textbf{Implementation}}\\
		\midrule
		\cite{CarmeloRCartiere.} & & Einführung einer formalen Sprache, die etwas einfacher als die \glqq pure\grqq{} Dirac-Notation ist und einfacher in Programme übersetzt werden kann\\
		\cite{daSilvaFeitosa.2016} & 31-45 & Kapitel aus \cite{Castor.2016}. Vorstellung der Grundbausteine einer Objektorientierten Programmiersprache für QCs, basierend auf (Featherweight) Java\\
		
		\midrule
		\multicolumn{3}{c}{\textbf{Testing (aufgeteilt in Verifikation und Validierung)}}\\
		\midrule
		\cite{Mahadev.03.04.2018}& & Beschreibung eines Protokolls zur (interaktiven) Verifizierung der Ergebnisse eines QCs durch einen klassischen Computer. Sehr langes und anspruchsvolles Paper, \\
		\cite{Gheorghiu.2019} & & \textit{Verification of Quantum Computation: An Overview of Existing Approaches} fast Probleme und Lösungsansätze zusammen. Sehr komplexes Paper. Wenn, dann sind Introduction und Conclusions interessant.\\
		\bottomrule
	\end{tabularx}
	\caption{Quellenliste für SE im Quantencomputing}
	\label{tab:qSE-im-QC}
\end{table}

\section{Allgemeine Aufgaben und Forderungen}
aus dem Abstract von \cite{CarmeloRCartiere.}: \textit{Quantum computing represents the future of computing systems, but the method for reasoning about it is still a mix of linear algebra and Dirac notation, two subjects \textbf{more suitable for physicists rather than for computer scientists and software engineers.}}

\subsection{Aktuelle Herausforderungen (wenn nicht anders spezifiziert, aus \cite{Grumbling.2019}, Chpt. 6)}
\textit{...new and different tools are required to support quantum operations, including}
\begin{itemize}
	\item programming languages, that enable programmers to describe QC algorithms
	\item compilers to analyze them and map them onto quantum hardware
	\item additional support to analyze, optimize, debug and test programs
\end{itemize}
Detailiertere Herausforderungen der einzelnen SE-Gebiete, die aus dem Paper \cite{Grumbling.2019} stammen, sind entsprechend markiert.

\subsubsection{Simulation}
Simulatoren nehmen eine wichtige Rolle im QC ein, Skalierbarkeit und \textit{Lenkbarkeit} stellen die großen Herausforderungen in diesem Bereich dar. Simulatoren können verwendet werden, um den erwarteten Output zu liefern und können als Referenz für Hardwaretests dienen.

\subsubsection{Specification, Verification, and Debugging}
\begin{itemize}
	\item Komplexität macht korrektes Design sehr schwierig
	\item Starre \textit{(intractable)} Simulatoren limitieren die ausgeführten Simulationen
	\item Messung führt zur Zustandsreduktion, konventionelles Debugging funktioniert daher aufgrund der Natur von QC nicht
\end{itemize}
Verification Problem: \textit{is it possible for a classical client to verify the answer provided by a quantum computer?}\\
Herausforderung für das SE: \textit{QC developers will always be implementing some programs for QC machines before they have been built, making this task especially problematic. This situation will lead to programs that cannot	be validated by direct execution.}

\subsubsection{Compiling}



\subsection{Aspekt für das \textbf{System}-Engineering}\label{systemEngineering}
Aus \cite{Miranskyy.}: \textit{\glqq However, we conjecture that QC will not replace CC in the short run. Rather, QCs will be integrated into a System of Systems (SoS), where QC-based components will solve BQP problems (that CC cannot solve), while the solution will be passed to CC components for post-processing.\grqq{}}\\
Nach \cite{Roth.2015} hat ein \textit{System of Systems} folgende wesentliche Merkmale:
\begin{enumerate}
	\item Operative Unabhängigkeit der Elemente
	\item Unabhängiges Management der Elemente
	\item Evolutionäre Entwicklung
	\item Emergentes Verhalten (\textit{spontane Herausbildung von neuen Eigenschaften [...] infolge des Zusammenspiels)}
	\item Geographische Verteilung
\end{enumerate}

Sowohl Verteilung, als auch Unabhängigkeiten lassen sich gut an den Schemata von IBM und \cite{Grumbling.2019} erkennen, die bereits im ersten Dokument vorgestellt wurden:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.3\textwidth]{Bilder/schema-nach-qc_progress-and-prospects.png}
	\caption{Schematische Darstellung eines Quantencomputers nach \cite{Grumbling.2019}}
	\label{fig:schema-nach-qc_pro&pro}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=1.0\textwidth]{Bilder/schema-ibm-qc.png}
	\caption{Zuordnung der Komponenten des IBM-Quantencomputers aus zu den einzelnen Planes nach \cite{Grumbling.2019}}
	\label{fig:ibm-qc-plane-zuordnung}
\end{figure}


\subsection{Aufgaben für das Softwareengineering}
Fragen, die sich für das Softwareengineering stellen(\cite{Miranskyy.}):
\begin{itemize}
	\item Which of the SE practices that we use in the CC domain can be ported to the QC domain?
	\item Which of the practices are not applicable?
	\item Which novel practices should be created to address QC domain challenges?
\end{itemize}

Diese Fragen sind für die einzelnen Schritte des Entwicklungszyklus zu beantworten:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{Bilder/se-lifecycle.png}
	\caption{Entwicklungszyklus aus Vorlesungsfolien Softwareengineering v. Prof. Dr. G. Hagel}
	\label{fig:entwicklungszyklus-allgemein}
\end{figure}

\section{Initial Planning}

\section{Planning}

\section{Business Modelling}

\section{Requirements}

\section{Analysis \& Design}
\subsection{\cite{Leymann.2019}: Pattern}
\cite{Leymann.2019} versucht sich an der Einführung einer Patternsprache. Herangehensweise: \textit{Pattern werden bisher als \glqq Tricks\grqq{} gesammelt. Diese Tricks sollten formal als Muster notiert und wiederverwendet werden.}. Für die Arbeit vielleicht interessant ist außerdem die These, die Leymann aufstellt: 
\\
\textit{[...]computer scientists and software developers used to solve classical problems need a lot of assistance when being assigned to build quantum algorithms. To support and guide people in creating solutions in various domains, pattern languages are established.}
\\
Leymann beschreibt den Aufbau eines Patterns und bietet einen Grundstock an Mustern an, die sich verschiedenen Aufgaben widmen:
\begin{itemize}
	\item Initialization (aka State Preparation)
	\item Uniform Superposition
	\item Creating Enganglement
	\item Function Table
	\item Oracle (aka Black Box)
	\item Uncompute (aka Unentangling aka Copy-Uncompute)
	\item Phase Shift
	\item Amplitude Amplification
	\item Speedup via Verifying
	\item Quantum-Classic Split
\end{itemize}

\section{Implementation}

\subsection{Programming languages aus \cite{Grumbling.2019}}
Sicht auf Programmiersprachen: unterstützen den Prozess, die mathematische Beschreibung einer Problemlösung auf einen Computer zu übertragen.\\
Da es verschiedene Level an Abstraktion in der Beschreibung von Algorithmen gibt, werden auch verschieden abstrakte Sprachen benötigt:
\begin{itemize}
	\item Highest Level: Schnelle Entwicklung von algorithmen, verstecken von Hardwaredetails
	\item Lowest Level: Nahtlose interaktion mit der Hardware, komplette Beschreibung der möglichen, physikalischen Instruktionen
\end{itemize}

\subsection{Einführen einer Zwischenstufe zwischen Dirac und Code \cite{CarmeloRCartiere.}}
Das Paper beschäftigt sich mit der Einführung von Typen und Operatoren zur Erweiterung der Dirac-Notation. An dem Beispiel des Deutsch-Algorithmus wird der Übergang zwischen der vorgeschlagenen Notation und dem Code verdeutlicht. \textbf{Für das Softwareengineering interessant: die Notation erinnert stark an Pseudocode und kann dementsprechend diese Aufgaben übernehmen (Plattform- und Programmiersprachenunabhängige Darstellung von Algorithmen).}

\subsection{Design einer OOP für QC, basierend auf Featherweight Java \cite{daSilvaFeitosa.2016}}
Bisherige Programmiersprachen für QC sind imperativ oder funktional. In diesem Paper werden die grundlegenden Bausteine von FJQuantum vorgestellt, einer Objektorientierten Programmiersprache für QCs, \textit{created to reason and to develop programs handling quantum data and quantum operations, taking advantage of the characteristics of that paradigm}.\\
Folgende Einschränkungen müssen beachtet werden:
\begin{itemize}
	\item Es wird \textbf{Featherweight} Java verwendet, nicht Java. Die damit einhergehenden Einschränkungen (z.b. ist jede Instanz unveränderbar. Bei verändernden Operationen wird eine \textbf{neue Instanz} zurückgegeben) müssen beachtet werden
	\item Schon die Umsetzung eines Hadamard-Gates (S. 41, ab Zeile 11) scheint fehlerhaft / nicht komplett. Es wird lediglich eine Transition von klassischen Zuständen (im Code als boolean Parameter dargestellt) zum Bell-State angeboten, was nur einem kleinen Teil des Hadamard-Gates entspricht.
\end{itemize}
Aus den Einschränkungen ergeben sich (für mich) folgende Fragen:
\begin{itemize}
	\item Während die Optimierung von ausführbaren Dateien für klassische Computer eher nebensächlich ist, muss die Eingabe in einen Quantencomputer hochoptimiert sein, um mit den eingeschränkten Ressourcen (zeitlich wegen Flüchtigkeit, Größe wegen Anzahl QBits) zurechtzukommen. \textbf{Inwiefern kann OOP überhaupt für reale QCs genutzt werden?}
	\item Während Qiskit und Q\# in den letzten Jahren verwendet und weiterentwickelt wurden, scheint dieser Objektorientierte Ansatz nicht mehr aktiv zu sein. \textbf{Welche Erkenntnisse können trotzdem verwendet werden? Was heißt das für den objektorientierten Ansatz unabhängig von FJQuantum?}
\end{itemize}
Schlussfolgerung: Ich halte FJQuantum nicht für verwendbar. Die Frage, ob ein objektorientierter Ansatz Vorteile in der Darstellung von QC-Algorithmen bringt, bleibt bestehen.

\subsection{Qiskit}

\subsection{Q\#}

\section{Test (\cite{Miranskyy.})}

\subsection{White- and Black-Box testing}
\begin{itemize}
	\item Blackbox testing ist möglich: Bekomme ich den erwarteten Output für den gegebenen Input?
	\item Whitebox testing ist nur teilweise möglich. Code Review und Code inspection sind möglich, interactive debugging auf echter Hardware ist nicht möglich, da QCs \textit{black-box by design} sind.
	\item Whitebox testing mit Simulatoren ist möglich, z.b. xUnit für Q\#-Programme.
\end{itemize}

\subsection{Validierung (Folgt der Code dem Requirements-Dokument?)}
Aus \cite{Miranskyy.}: \textit{\glqq Essentially, the ease of validation will depend on the difficulty of implementing a program for validating the results and the time needed to execute the validation. \grqq{}}\\
Je nach Komplexität der Validierung bietet sich eine Implementierung des Validierungsprogramms auf einem klassischen oder einem Quantencomputer an. Hierfür werden in dem Paper verschiedene Beispiele genannt:
\begin{itemize}
	\item Shors Algorithmus läuft auf einem QC in polynimieller Zeit, die Validierung wächst linear. Eine Validierung auf einem klassischen Computer ist zu bevorzugen.
	\item \textit{Boson sampling} wächst exponentiell
\end{itemize}

\subsection{Verifikation (Folgt der Code dem Designdokument?)}
\begin{enumerate}
	\item Testen auf einem Simulator $\rightarrow$ geht davon aus, dass der Simulator richtig funktioniert
	\item Mehrfaches Testen auf \textbf{einem} echten QC $\rightarrow$ \textit{mehrfach} nötig durch die probabilistische Natur von QCs, geht davon aus, dass dieser eine QC korrekt funktioniert
	\item Mehrfaches Testen auf \textbf{verschiedenen} echten QCs
\end{enumerate}
Um diese Tests durchzuführen wurde in \cite{Mahadev.03.04.2018} ein Protokoll vorgestellt, dessen Implementierung durch die benötigten Ressourcen allerdings noch nicht in Aussicht steht.

\subsubsection{Das Verification-Problem}
Aus \cite{Grumbling.2019}: \textit{the verification problem asks the question is it possible for a classical client to verify the answer provided by a quantum computer?}\\
\\
\cite{Grumbling.2019} unterscheidet 3 Modelle


\subsection{Verification of Quantum Computation: An Overview of Existing Approaches \cite{Gheorghiu.2019}}
Aus \cite{Gheorghiu.2019}, abstract: \textit{\glqq Quantum computers promise to efficiently solve not only problems believed to be intractable for classical computers, but also problems for which verifying the solution is also considered intractable.\grqq{}}\\
Das Paper fasst anschließend bestehende Verfahren und Protokolle zusammen. Aus den \textit{Conclusions} (S. 788f): \textit{\glqq [...]none of them achieve the ultimate goal of the field, which is to have a classical client verify the computation performed by a single quantum server[...]\grqq{}}.\\
\textit{\glqq What all of the surveyed approaches have in common, is that none of them are based on computational assumptions. In other words, they all perform verification unconditionally. However, recently, there have been attempts to reduce the verifier’s requirements by incorporating computational assumptions as well.\grqq{}}\\
\textit{\glqq [...]the problem of a classical verifier delegating computations to a single prover is the main open problem of the field,[...]\grqq{}}

\subsection{Wahrgenommene Struktur des Gebiets Testing}
\begin{enumerate}
	\item Testing: allgemeines Kapitel
	\begin{itemize}
		\item Black-Box-Testing lässt sich einfach übertragen, White-Box-Testing nur eingeschränkt mit bestimmten Simulatoren möglich (also nicht für große Algorithmen geeignet) \cite{Miranskyy.}
		\item Testing lässt sich aufteilen in Verifikation und Validierung. Validierung: Folgt der Code dem Requirementsdokument? Verifikation: Folgt der Code dem Designdokument? \cite{Miranskyy.}
		\item Als bestimmendes Problem hat sich das \textit{Verification problem} herausgestellt: \textit{is it possible for a classical client to verify the answer provided by a quantum computer?} \cite{Grumbling.2019}, S. 146
	\end{itemize}
	\begin{enumerate}
		\item Das Verification Problem:
		\begin{itemize}
			\item Das Verifikationsproblem kurz zu erklären ist nicht schwierig: wie kann ein Gerät, das Probleme einer mächtigeren Komplexitätsklasse löst, einem klassischen Gerät \textbf{versichern}, dass das Ergebnis korrekt ist?
			\item 3 Modelle aus \cite{Grumbling.2019}, S. 147 \textit{kurz} erklären?
			\item jedes tiefere Vorstoßen in eine der in \cite{Grumbling.2019}, S. 147 angesprochenen Modelle führt zu relativ komplexen Quellen
		\end{itemize}
		\item Genaue Ausführung verschiedener Ansätze:
		\begin{itemize}
			\item \cite{Gheorghiu.2019}: Komplettes Paper beschäftigt sich mit verschiedenen Ansätzen + beispielen
			\item \cite{Liu.2019}: Anwendung des \href{https://de.wikipedia.org/wiki/Hoare-Kalk\%C3\%BCl}{Hoare-Kalkül} auf Quantenprobleme (mit, wie das enthaltene Statistikkapitel erahnen lässt, immensem Aufwand).
			\item \cite{Ying.2019}: Weiterer Ansatz, der \textit{Quantum Hoare logic} beinhaltet
			\item \cite{Anticoli.2016}: Übertragung von Quipper-Programmen (Quipper: \cite{Smith.01.12.2014}) auf QPMC (quantum protocol model checker, based on density matrix formalism): \textit{The main idea is to use this framework to create a tool that allows, on the on ehand, the description of quantum algorithms and protocols in an high-level programming language, and on the other hand their formal verification.} (aus den Conclusions von \cite{Anticoli.2016})
		\end{itemize}
		\item \textit{Conclusion}
		\begin{itemize}
			\item \cite{Grumbling.2019}: Markierung auf S. 148: \textit{In general, however, the above techniques represent small and inadequate inroads into a largely unmapped space of challenges. The challenges of debugging QC systems—and more specifically the near-intractability of approaches like simulations or assertions—means that there remains a critical need to continue development of tools to verify and debug quantum software and hardware.}
			\item Conclusions aus \cite{Miranskyy.}
			\item ....
		\end{itemize}
	\end{enumerate}
\end{enumerate}

\section{Deployment}

\section{Evaluation}

\section{Config \& Change Management}

\section{Project Management}
In der Praxis müssen Quantencomputer und Quantum Annealer von Experten betreut und gewartet werden. Diese bieten Rechenzeit und Know-How als Dienstleistung an. In folgendem Artikel stellt D-Wave Quantencomputer her und betreut diese selbst. Sigma-i kauft Rechenzeit und paart diese mit Know-How zu eigenen Dienstleistungen: \href{https://www.dwavesys.com/press-releases/sigma-i-and-d-wave-announce-largest-ever-quantum-cloud-access-contract}{Sigma-i als Dienstleister für D-Wave-Annealer}.

\section{Environment}
Wie bereits in \nameref{systemEngineering} durch \cite{Miranskyy.} erläutert, werden Quantencomputer klassische Computer vorerst nicht verdrängen sondern ergänzen. Es entsteht ein \textit{System of Systems} mit den genannten Merkmalen als Umgebung.